"""data processing module"""

from lxml import etree


START_OF_DATA = '1.'


def get_extra_data(reader):
    """Get extra variables from a CSV file.

    CSV file can store extra variables as key value pairs in
    first two columns such that first columns stores variable name
    and the second column is the value.
    Those variables can be stored anywhere before the START_OF_DATA
    mark and only known variable names will be processed.
    """
    result = {}
    variables = ('ImateljId', 'StvarateljId')

    for line in reader:
        v = line[0].strip()
        if v in variables:
            result[v] = line[1].strip()

        # variables can't be stored after START_OF_DATA
        if v == START_OF_DATA:
            break

    return result


def get_rows(reader):
    rows = []
    _started = False
    for row in reader:
        # skip lines before START_OF_DATA
        if not _started and row[0].strip() == START_OF_DATA:
            _started = True

        if _started:
            rows.append(row)

    return rows


def generate_xml(data, extra_data):
    xml = etree.Element('ARHiNET', xmlns="http://arhinet.arhiv.hr/ARHiNET.xsd")

    for rj in data:
        el = etree.Element('RegistraturnaJedinica', **rj.xml_attributes())
        if rj.vrsta_zapisa_id:
            el.append(etree.Element('Gradja', KomPodvrstaId=rj.vrsta_zapisa_id))
        if rj.vrsta_medija_id:
            el.append(etree.Element('Medij', VrstaMedijaId=rj.vrsta_medija_id))
        if rj.komada:
            el.append(etree.Element('KolicinaArhivskihJedinica',
                                    MjernaJedinicaId='1',
                                    Kolicina=rj.komada))
        if rj.kolicina:
            el.append(etree.Element('KolicinaTehnickihJedinica',
                                    Kolicina=str(rj.kolicina),
                                    VrstaTehnickeJediniceId=rj.teh_jed_id))

        if rj.level == 1:
            if 'ImateljId' in extra_data:
                el.set('ImateljId', extra_data['ImateljId'])
            xml.append(el)
            if 'StvarateljId' in extra_data:
                el.append(etree.Element('Stvaratelj',
                                        StvarateljId=extra_data['StvarateljId'],
                                        UlogaId='1'))
        else:
            xml_parent = xml.find(rj.get_parent_xpath())
            if xml_parent is not None:
                xml_parent.append(el)
            else:
                raise ValueError('Node {} is missing parent {}'.format(rj.path, rj.parent_path))

    return xml
