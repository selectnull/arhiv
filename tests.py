import unittest

from arhinet import RegistraturnaJedinica
from data import get_rows, get_extra_data
from decimal import Decimal


class TestDataReader(unittest.TestCase):
    def test_get_extra_data(self):
        input_reader = [
            ['Foo', 'Bar'],
            ['StvarateljId', '123'],
            ['ImateljId', '456'],
        ]
        self.assertEqual(
            get_extra_data(input_reader),
            {'StvarateljId': '123', 'ImateljId': '456'}
        )

    def test_get_rows(self):
        input_reader = [
            ['1'],
            ['This is a title'],
            [''],
            ['1.', None, None, 'Row 1'],
            ['1.1.', None, None, 'Row 1.1'],
            ['2.', None, None, 'Row 2'],
        ]
        data = get_rows(input_reader)

        self.assertEqual(data[0][0], '1.')
        self.assertEqual(len(data), 3)

class TestRegistraturnaJedinica(unittest.TestCase):
    def setUp(self):
        self.path_tests = [
            ['1.4.9', ''],
            ['1.4.9.', ''],
        ]

    def test_path(self):
        for x in self.path_tests:
            self.assertEqual(RegistraturnaJedinica(x).path, '1.4.9.')

    def test_level(self):
        for x in self.path_tests:
            self.assertEqual(RegistraturnaJedinica(x).level, 3)

    def test_position(self):
        for x in self.path_tests:
            self.assertEqual(RegistraturnaJedinica(x).position, 9)

    def test_parent_path(self):
        for x in self.path_tests:
            self.assertEqual(RegistraturnaJedinica(x).parent_path, '1.4.')

    def test_parent_path_of_first_level_is_none(self):
        self.assertEqual(RegistraturnaJedinica(['1', '']).parent_path, '')

    def test_data(self):
        data = [
            '1.4.9.1', '2001.', '2002.', 'this is a title',
            2.2, 2.2, 1.2, 'knjiga'
        ]
        rj = RegistraturnaJedinica(data)

        self.assertEqual(rj.godina_od, 2001)
        self.assertEqual(rj.godina_do, 2002)
        self.assertEqual(rj.naziv, 'this is a title')
        self.assertEqual(rj.komada, Decimal(2.2))
        self.assertEqual(rj.mj_jed, Decimal(2.2))
        self.assertEqual(rj.kolicina, Decimal(1.2))
        self.assertEqual(rj.teh_jed, 'knjiga')

    def test_get_parent_xpath(self):
        r = RegistraturnaJedinica(['1.4.9.1', ''])
        self.assertEqual(
            r.get_parent_xpath(),
            'RegistraturnaJedinica[1]/RegistraturnaJedinica[4]/RegistraturnaJedinica[9]'
        )

        r = RegistraturnaJedinica(['1.1', ''])
        self.assertEqual(r.get_parent_xpath(), 'RegistraturnaJedinica[1]')


if __name__ == '__main__':
    unittest.main()
