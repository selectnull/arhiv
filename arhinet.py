# coding=utf-8
""" arhinet format utilities """

import re
import sys
from decimal import Decimal, InvalidOperation


def log_error(msg, *args):
    sys.stderr.write(msg.format(*args))


class RegistraturnaJedinica(object):
    def __init__(self, line):
        self.parse_path(line[0])
        self.parse_data(line[1:])

    def parse_path(self, line):
        """Parse signature.

        * input argument is dot delimited hierarchy of numbers, eg 1.4.9
          with or without trailing dot.
        * path is dot delimited first part of input argument
          normalized with trailing dot ('1.4.9.')
        * level is depth of path delimited hierarchy (3)
        * position is position inside current node (9)
        * parent_path is path of the parent node
        """

        path = line
        if not path.endswith('.'):
            path += '.'
        split_path = [x for x in path.split('.') if x.strip()]
        if all([int(x) for x in split_path]):
            self.path = path
            self.level = len(split_path)
            self.position = int(split_path[-1])
            self.parent_path = '.'.join(split_path[:-1])
            # make sure parent_path ends with '.'
            # first level of hierarchy has emptry string for parent
            if self.parent_path:
                self.parent_path += '.'
        else:
            raise ValueError('Bad input: {}'.format(line))

    def parse_data(self, data):
        def get_value_or_none(seq, idx):
            try:
                return seq[idx]
            except IndexError:
                return None

        def get_year(seq, idx):
            v = get_value_or_none(seq, idx)
            try:
                return int(v[:4])
            except (TypeError, ValueError):
                return None

        self.godina_od = get_year(data, 0)
        self.godina_do = get_year(data, 1)
        if self.godina_od and self.godina_do and self.godina_od > self.godina_do:
            log_error('{} -- Godina Od ({}) je veca od Godina Do ({})\n',
                      self.path, self.godina_od, self.godina_do)

        self.naziv = get_value_or_none(data, 2) or ''

        self.komada = get_value_or_none(data, 3)
        self.mj_jed = get_value_or_none(data, 4)
        try:
            self.kolicina = Decimal(get_value_or_none(data, 5))
        except (TypeError, InvalidOperation):
            self.kolicina = None

        self.teh_jed = get_value_or_none(data, 6)
        self.teh_jed_id = self.get_tehnicka_jedinica_id(self.teh_jed)

        self.vrsta_medija = get_value_or_none(data, 7)
        self.vrsta_medija_id = self.get_vrsta_medija_id(self.vrsta_medija)

        self.vrsta_zapisa = get_value_or_none(data, 8)
        self.vrsta_zapisa_id = self.get_vrsta_zapisa_id(self.vrsta_zapisa)

    def xml_attributes(self):
        attrs = {}
        # we skip real level 1 (podfond)
        razina = 0 if self.level == 1 else self.level
        attrs['RazinaId'] = str(razina)
        attrs['Signatura'] = str(self.position)
        if self.godina_od:
            attrs['GodinaOd'] = str(self.godina_od)
        if self.godina_do:
            attrs['GodinaDo'] = str(self.godina_do)
        if self.naziv:
            attrs['Naziv'] = self.naziv

        return attrs

    def get_vrsta_medija_id(self, vrsta_medija):
        if not vrsta_medija:
            return None
        vrsta_medija = vrsta_medija.strip()
        data = {
            u"papir": "1",
            u"pergamena": "2",
            u"mikrooblik": "3",
            u"fotografija": "4",
            u"dijapozitiv": "7",
            u"filmska vrpca": "8",
            u"magnetofonska traka": "9",
            u"digitalni medij": "10",
            u"video kazeta": "11",
            u"audio kazeta": "12",
            u"staklo": "13",
            u"predmet": "14",
            u"gramofonska ploča": "15",
            u"platno": "16",
        }
        return data[vrsta_medija]

    def get_vrsta_zapisa_id(self, vrsta_zapisa):
        if not vrsta_zapisa:
            return None
        vrsta_zapisa = vrsta_zapisa.strip()
        data = {
            u'dokument': '40'
        }
        return data[vrsta_zapisa]

    def get_tehnicka_jedinica_id(self, teh_jed):
        if not teh_jed:
            return None
        teh_jed = teh_jed.strip()
        data = {
            u'album': '2',
            u'ambalažna kutija': '4',
            u'audio kazeta': '7',
            u'bilježnica': '10',
            u'komad': '12',
            u'CD': '16',
            u'disk': '22',
            u'fascikl': '23',
            u'mikrooblik II': '24',
            u'kutija': '26',
            u'DVD': '27',
            u'film': '32',
            u'filmski svitak': '33',
            u'foto-album': '36',
            u'gramofonska ploča': '39',
            u'kartoteka': '42',
            u'knjiga': '43',
            u'omotnica': '48',
            u'ladica': '56',
            u'list': '61',
            u'magnetofonska traka': '63',
            u'mapa': '67',
            u'mikrofilmski svitak': '69',
            u'neg.': '74',
            u'ormar*': '80',
            u'registrator': '91',
            u'svitak*': '94',
            u'sanduk*': '97',
            u'svezak': '105',
            u'svežanj': '108',
            u'vrpca': '118',
            u'video': '121',
            u'vrećica': '122',
        }
        return data[teh_jed]

    def get_parent_xpath(self):
        xp = []
        parents = [pos for pos in self.parent_path.split('.') if pos]
        for x in parents:
            xp.append('RegistraturnaJedinica[{}]'.format(x))

        return '/'.join(xp)

    def __unicode__(self):
        return u'{} - {}'.format(self.path)
